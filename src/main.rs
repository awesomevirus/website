#![allow(non_snake_case)]

use dioxus::prelude::*;
use tracing::Level;

#[derive(Clone, Routable, Debug, PartialEq)]
enum Route {
    #[route("/")]
    Home {},
    #[route("/cereal/")]
    Cereal {},
}

fn main() {
    // Init logger
    dioxus_logger::init(Level::INFO).expect("failed to init logger");
    launch(App);
}

fn App() -> Element {
    rsx! {
        Router::<Route> {}
    }
}

#[component]
fn Cereal() -> Element {
    rsx! {
        Link { to: Route::Home {}, "Go to counter" }
        "Blog post"
    }
}

#[component]
fn Home() -> Element {
    rsx! {
        div {
            Link {
            to: Route::Cereal {},
            "Go to blog"
        }
        div {
            h1 { "The Free Cereal" }
            i { "An open cereal company."}
        }
        div {
            class: "FlexContainer",        
            InfoBox { title: "You know what's inside.", text: "No secrets. No surprises"}
            InfoBox { title: "Public Domain cereal manufacturing", text: "You too can replicate Dino Dump cereal at home."}
            InfoBox { title: "Owned by you.", text: "Dino Dumps are fully owned by the people that make it what it is today. You."}}
        }
        div {
            p {
                class: "Information",
                "Our cereal manufacturing process is fully free to download, to distribute and utilise yourself.
                Dino Dumps provides the world's first fully transparent cereal manufacturing process."
            }
        }
    }
}

#[derive(PartialEq, Props, Clone)]
struct InfoBoxContents {
    title: String,
    text: String,
}

fn InfoBox(contents: InfoBoxContents) -> Element {
    rsx!(
        div {
            class: "InfoBox",
            h2 {
                "{contents.title}"
            }
            p {
                "{contents.text}"
            }
        }
    )
}
